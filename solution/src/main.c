#include "bmp/bmp.h"
#include "file/file.h"
#include "image/image.h"
#include "rotate/rotate.h"

#include <err.h>
#include <stdio.h>

int main(int args, char **argv) {
    if (args != 3) err(1, "Wrong amount of arguments");
    const char *file_input = argv[1];
    const char *file_output = argv[2];
    FILE *in = NULL;
    FILE *out = NULL;

    if (!file_open(&in, file_input, "rb") ||
        !file_open(&out, file_output, "wb")) {
        err(1, "Can't open");
    }

    struct image image = {0, 0, NULL};
    if (from_bmp(in, &image) != READ_OK) {
        image_destroy(image);
        file_close(&in);
        err(1, "Can't read");
    }

    struct image rotated_image = rotate(&image);
    if (to_bmp(out, &rotated_image) != WRITE_OK) {
        image_destroy(image);
        image_destroy(rotated_image);
        file_close(&in);
        err(1, "Can't write");
    }

    if (!file_close(&in) || !file_close(&out)) {
        image_destroy(image);
        image_destroy(rotated_image);
        file_close(&in);
        err(1, "Can't close");
    }

    image_destroy(image);
    image_destroy(rotated_image);
    return 0;
}

