#include "file/file.h"

#include <stdio.h>

bool file_open(FILE **file, const char *name, const char *rights) {
    *file = fopen(name, rights);
    return (*file != NULL);
}

bool file_close(FILE* const* file) {
    return (!fclose(*file));
}
