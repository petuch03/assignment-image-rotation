#include <stdio.h>

#include "image/image.h"
#include "bmp/bmp.h"
#include "rotate/rotate.h"

#define BMP_TYPE 0x4D42
#define BMP_PLANES 1
#define BITMAP_INFO_HEADER 40

static enum read_status read_header(FILE *in, struct bmp_header *header) {
    if (fseek(in, 0, SEEK_END) != 0) {
        return READ_STREAM_TROUBLE;
    }
    size_t size = ftell(in);
    if (size < sizeof(struct bmp_header)) return READ_INVALID_HEADER;

    rewind(in);
    if (fread(header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_NMEMD_FROM_STREAM_TROUBLE;
    }
    return READ_OK;
}

static enum read_status read_pixels(struct image *img, FILE *in, uint8_t padding) {
    struct image output = image_create(img->width, img->height);
    size_t width = img->width;
    size_t height = img->height; // для удобства
    struct pixel *pixels = output.pixels;
    for (size_t i = 0; i < height; i++) {
        if (fread(pixels + i * width, sizeof(struct pixel), width, in) != width) {
            return READ_NMEMD_FROM_STREAM_TROUBLE;
        }
        if (fseek(in, padding, SEEK_CUR) != 0) {
            return READ_STREAM_TROUBLE;
        }
    }
    img->pixels = pixels;
    return READ_OK;
}


static uint8_t get_padding(uint32_t width) { return width % 4; }


enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header = {0};
    if (read_header(in, &header) == READ_INVALID_HEADER) return READ_INVALID_SIGNATURE;
    if (header.bfType != BMP_TYPE) return HEADER_INVALID_BMP_TYPE;
    if (header.biPlanes != BMP_PLANES) return HEADER_INVALID_BMP_PLANES;
    if (header.biSize != BITMAP_INFO_HEADER) return HEADER_INVALID_BITMAP_INFO_HEADER;
    img->width = header.biWidth;
    img->height = header.biHeight;
    return read_pixels(img, in, get_padding(header.biWidth));
}

static struct bmp_header create_header(const struct image img) {
    const uint32_t img_size = (sizeof(struct pixel) * img.width + get_padding(img.width)) * img.height;
    struct bmp_header header = {
            .bfType = BMP_TYPE,
            .bfileSize = sizeof(struct bmp_header) + img_size,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BITMAP_INFO_HEADER,
            .biWidth = img.width,
            .biHeight = img.height,
            .biPlanes = BMP_PLANES,
            .biBitCount = 24,
            .biCompression = BI_RGB,
            .biSizeImage = img_size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,

    };
    return header;
}

enum write_status write_image(struct image const *img, FILE *out, uint8_t padding) {
    size_t width = img->width;
    size_t height = img->height;
    const uint64_t zero = 0;
    uint64_t cnt = 0;
    for (size_t i = 0; i < height; ++i) {
        size_t const write_result = fwrite(img->pixels + i * width, sizeof(struct pixel), width, out);
        if (write_result != width) {
            return WRITE_STREAM_TROUBLE;
        }
        cnt += (uint64_t) write_result;
        size_t const write_result_zero = fwrite(&zero, 1, padding, out);
        if (write_result_zero != padding) {
            return WRITE_STREAM_TROUBLE_ZERO;
        }
    }
    if (cnt == height * width) return WRITE_OK;
    return WRITE_ERROR;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    struct bmp_header header = create_header(*img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) return WRITE_ERROR;
    uint8_t padding = get_padding(header.biWidth);
    return write_image(img, out, padding);
}
