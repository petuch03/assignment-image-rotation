#include "rotate/rotate.h"

#include <stdlib.h>

struct image image_create(size_t width, size_t height) {
    struct image output = {0, 0, NULL};
    output.width = width;
    output.height = height;
    output.pixels = malloc(width * height * sizeof(struct pixel));
    return output;
}

static void recalculate(struct image * output, struct image const *source, int row, int column) {
    output->pixels[row * output->width + column] = (source->pixels[(source->height - column - 1) * source->width + row]);
}

void image_destroy(struct image to_erase) {
    free(to_erase.pixels);
}

struct image rotate(struct image const *source) {
    struct image output = image_create(source->height, source->width);
    for (size_t i = 0; i < source->width; i++) {
        for (size_t j = 0; j < source->height; j++) {
            recalculate(&output, source, i, j);
            //output.pixels[i * output.width + j] = (source->pixels[(source->height - j - 1) * source->width + i]);
        }
    }
    return output;
}
