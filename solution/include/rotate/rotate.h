#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H

#include "image/image.h"

struct image rotate(struct image const *source);
struct image image_create(size_t width, size_t height);
void image_destroy(struct image to_erase);

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
