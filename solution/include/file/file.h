#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

//enum file_status {
//    OPENED = 0,
//    CLOSED = 0,
//    ERROR
//    /* коды других ошибок  */
//};
bool file_open(FILE **file, const char *name, const char *rights);

bool file_close(FILE* const* file);

#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_H
