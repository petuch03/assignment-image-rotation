#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>
#include <stdio.h>

struct image {
    uint64_t width, height;
    struct pixel *pixels;
};

struct pixel {
    uint8_t b, g, r;
};

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
