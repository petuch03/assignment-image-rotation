#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "image/image.h"

#include  <stdint.h>

struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} __attribute__((packed));

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_STREAM_TROUBLE,
    READ_NMEMD_FROM_STREAM_TROUBLE,
    HEADER_INVALID_BMP_TYPE,
    HEADER_INVALID_BMP_PLANES,
    HEADER_INVALID_BITMAP_INFO_HEADER
    /* коды других ошибок  */
};

enum read_status from_bmp(FILE *in, struct image *img);

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_STREAM_TROUBLE,
    WRITE_STREAM_TROUBLE_ZERO
    /* коды других ошибок  */
};

enum write_status to_bmp(FILE *out, struct image const *img);

enum compression_types {
    BI_RGB = 0,
    BI_RLE8 = 1,
    BI_RLE4 = 2,
    BI_BITFIELDS = 3,
    BI_JPEG = 4,
    BI_PNG = 5,
    BI_ALPHABITFIELDS= 6,
};

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
